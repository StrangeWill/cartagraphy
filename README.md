#CARTAgraphy app
CARTAgraphy provides an SMS interface to CARTA’s [bus tracker API](http://bustracker.gocarta.org).

---

Clone the repository, run `npm install`, then `node server.js`.

Use Postman (or similar) to send a POST request to `localhost:8080/api/incoming` with the following key/value pairs* in the request body (x-www-form-urlencoded):

- `Body` : e.g. `When does #4 arrive at Hamilton Mall?`
- `From`   : e.g. `4235550099`

_*Note that `Body` and `From` above follow the Twilio convention. These are interchangeable with `message` and `phone` respectively._

##Git Submodules
This repository uses a git submodule to track sensitive information like API keys in a private repository. After cloning the `cartagraphy` repo, run `git submodule init` and `git submodule update` to clone the necessary submodule.

You’ll need access to the private `mrkp/cartagraphy_keys` repository on Bitbucket. Please email <alfonso@mrkp.co> if you want to contribute to this repository and need access to the keys.

Alternatively, you could create your own keys repository and change the `.gitmodules` file in this repo to point to your own private repository. The following file needs to be at the root of your keys repository:

### carta.js
This file should look like this:

    var carta_key = "your_carta_api_key_here";
    module.exports = carta_key;

Or equivalent.

For information on acquiring an API key, see [this GitHub issue.](https://github.com/gocarta/gtfs/issues/10)

---

CARTAgraphy is published under the [MIT License](http://choosealicense.com/licenses/mit/).
