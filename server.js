// DEPENDENCIES
// =============================================================================
var express      = require('express'),   // call express
    app          = express(),            // define our app using express
    bodyParser   = require('body-parser'),
    config       = require('./config.js'),
    fs           = require('fs'),
    natural      = require('natural')
    ;

// GLOBAL VARIABLES
// =============================================================================
global.stopsClassifier = new natural.BayesClassifier();
global.modifiedRoutes = {};
global.stopsCollection = {};
global.shuttleRoutes = [];
global.busroutes = JSON.parse(fs.readFileSync('./app/data/routes/routes.json', 'utf8'));
global.validRoutes = [];
global.validRoutesConvo = [];

// LOAD DATABASE
// =============================================================================
require('./app/models/db.js');

// BODY PARSER
// =============================================================================
// (This will let us get the data from a POST request.)
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// LOAD CLASSIFIER
// =============================================================================
require('./app/modules/setup/classifierSetup.js');

// LOAD BUS ROUTES DATA
// =============================================================================
require('./app/modules/setup/busroutesSetup.js');

// LOAD ROUTES FOR OUR API
// =============================================================================
require('./app/routes.js')(app, express);

// SET THE PORT AND START THE SERVER
// =============================================================================
var port = process.env.PORT || config.port;    // set our port
app.listen(port);
console.log('Magic happens on port ' + port);
