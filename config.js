var config = {
  port              : 8080, // port at which application is listening
  convoExpiration   : 180,  // how long before a conversations expire, in seconds
  maxTries          : 3,    // maximum # of attempts to asist user before referring to voice line
  maxSuggestedStops : 6,    // maximum # of stops we’ll suggest/probe about
  CARTAvoicePhone   : "(423) 629-1473"  // CARTA voice line
};

module.exports = config;
