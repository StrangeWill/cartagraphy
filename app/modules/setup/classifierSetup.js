var csv = require('csv-to-json');

var stopsGtfs = csv.parse('./app/data/gtfs/stops.txt');

// Add documents for natural to train on for each stop:
stopsGtfs.forEach(function (stop) {
  global.stopsClassifier.addDocument(stop.stop_name.replace(/M\.L\. KING/g, 'MLK MLKING KING'), stop.stop_id);
  global.stopsCollection[stop.stop_id] = stop;
});

global.stopsClassifier.train();
