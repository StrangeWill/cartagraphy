var csv = require('csv-to-json'),
    conversation = require('../conversation.js')
    ;

// get data from csv file in gtfs repo
var routes = csv.parse('./app/data/gtfs/routes.txt'),
    routesPattern = new RegExp('([a-zA-Z])')
    ;

for (var index in routes) {
  // Check that this is an actual property and not from the prototype:
  if (routes.hasOwnProperty(index)) {
    var route = routes[index];

    // Only do this if the line isn’t empty:
    if (route.route_id !== '') {

      // Add route to validRoutes array:
      global.validRoutes.push(route.route_id);

      // Add route to validRoutesConvo array:
      // (add padding before the single-digit routes so we can sort them better later)
      global.validRoutesConvo.push(route.route_id.replace(/^(\d)$/, '0$1'));

      // If the route has a letter in it…
      if (routesPattern.test(route.route_id)) {
        // If the modifiedRoutes object doesn’t already have a property with this route number:
        if (!global.modifiedRoutes[route.route_id.replace(routesPattern, '')]) {
          // Create a property with this route number and add this letter modifier as the 1st item inside an array:
          global.modifiedRoutes[route.route_id.replace(routesPattern, '')] = [ route.route_id.match(routesPattern)[0] ];

        // If the modifiedRoutes object already has a property with this route number:
        } else {
          // Push this letter modifier to this route’s array of modifiers:
          global.modifiedRoutes[route.route_id.replace(routesPattern, '')].push(route.route_id.match(routesPattern)[0]);
        }
      }

      // If the route is determined to be a shuttle, add it to shuttleRoutes:
      if (/shuttle/i.test(route.route_long_name)) {
        global.shuttleRoutes.push(route.route_id);
      }
    }
  }
}

// create a nice version of the route list to use in conversations with the rider
conversation.makeBetterRouteList();
