var massageStops = {

  filter: function (sessionObj, cb) {
    // Define filteredStops as an array if it is still null:
    sessionObj.filteredStops = [];

    sessionObj.matchedStops.forEach(function (stop) {
      if (global.busroutes[sessionObj.routeNo].indexOf(parseInt(stop)) >= 0) {
        sessionObj.filteredStops.push(stop);
      }
    });

    if (sessionObj.filteredStops.length === 1) {
      sessionObj.definitiveStop = sessionObj.filteredStops[0];
    }

    if (typeof (cb) === 'function') {
      cb(null, sessionObj);
    }
  },

  findDuplicates: function (sessionObj, cb) {
    // Define duplicateStops as an array if it is still null:
    sessionObj.duplicateStops = {};

    // Set initial value of culledStops to filteredStops:
    // (It will be culled down directly below.)
    sessionObj.culledStops = sessionObj.filteredStops.slice();

    var rawStopNames = [];
    sessionObj.culledStops.forEach(function (stop) {
      rawStopNames[stop] = global.stopsCollection[stop].stop_name;
    });

    rawStopNames.forEach(function (stopName, index) {
      var foundPosition = rawStopNames.lastIndexOf(stopName);
      if (foundPosition !== index) {
        sessionObj.duplicateStops[index] = foundPosition;
        var filteredItem = sessionObj.culledStops.indexOf(foundPosition.toString());
        if (filteredItem !== -1) {
          sessionObj.culledStops.splice(filteredItem, 1);
        }
      }
    });

    if (sessionObj.culledStops.length === 1) {
      sessionObj.definitiveStop = sessionObj.culledStops[0];
    }

    // If no duplicate stops found, revert duplicateStops back to null:
    // (so that it returns false when checked)
    if (sessionObj.duplicateStops.length === 0) {
      sessionObj.duplicateStops = null;
    }

    if (typeof (cb) === 'function') {
      cb(null, sessionObj);
    }
  }

}

module.exports = massageStops;
