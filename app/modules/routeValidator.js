var conversation = require('./conversation.js')
    ;

var routeValidator = function (sessionObj, cb) {
  // First, check if it is a modified route (i.e. contains a letter), and a valid one at that.
  var modifiedRoutePattern = new RegExp(/(\d+)([a-zA-Z])/);
  sessionObj.routeNo = sessionObj.routeNo.toUpperCase();

  // Check that the user is not asking for a shuttle:
  if (sessionObj.routeNo !== 'SHUTTLE') {

  // Check if we have a route number with a letter modifier:
    if (modifiedRoutePattern.test(sessionObj.routeNo)) {
      var routeMatchResults = sessionObj.routeNo.match(modifiedRoutePattern);

      // If number section of route number is a known modified route…
      if (global.modifiedRoutes[routeMatchResults[1]]) {

        // … but none of the letters match up:
        if (global.modifiedRoutes[routeMatchResults[1]].indexOf(routeMatchResults[2].toUpperCase()) === -1) {
          sessionObj.routeNo = null;
          sessionObj.routeHints = [];
          global.modifiedRoutes[routeMatchResults[1]].forEach(function (letter) {
            sessionObj.routeHints.push(routeMatchResults[1] + letter);
          });
        }

      // If number section of route number is not a known modified route:
      } else {
        sessionObj.routeNo = null;
      }

    // It is not a shuttle or a modified route, so let’s see if it is valid:
    } else {
      // Cross reference with known bus routes:
      var found = false;
      for (var key in global.busroutes) {
        if (key === sessionObj.routeNo) {
          found = true;
        }
      }

      // If it hasn’t been found, check to see whether it’s a modified route that’s missing a letter:
      if (!found) {
        if (global.modifiedRoutes[sessionObj.routeNo]) {
          sessionObj.routeHints = [];
          global.modifiedRoutes[sessionObj.routeNo].forEach(function (letter) {
            sessionObj.routeHints.push(sessionObj.routeNo + letter);
          });
        }
        // and set the session route number to null:
        sessionObj.routeNo = null;
        sessionObj.validRoute = false;
      }
    }
  }

  if (typeof (cb) === 'function') {
    cb(null, sessionObj);
  }
}

module.exports = routeValidator;
