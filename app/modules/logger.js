var fs = require('fs'),
    moment = require('moment')
    ;

exports.log = function (sessObj, message) {
  var logtime = moment(new Date());
//  var timeDiff = moment().subtract(sessObj.startTime).milliseconds();
  var timeDiff = logtime.diff(sessObj.startTime);
//  var totalTimeDiff = moment().subtract(sessObj.convoStartTime).seconds();
  var totalTimeDiff = logtime.diff(sessObj.convoStartTime);
  var totalTimeDiffString;
  if (totalTimeDiff < 1000) {
    totalTimeDiffString = totalTimeDiff + ' milliseconds.';
  } else if (totalTimeDiff < 60000) {
    totalTimeDiffString = totalTimeDiff / 1000 + ' seconds.';
  } else {
    totalTimeDiffString = totalTimeDiff / 60000 + ' minutes.';
  }
  var matchedStops;
  if (sessObj.matchedStops.length > 6) {
    matchedStops = 'Greater than 6 matched stops.'
  } else {
    matchedStops = JSON.stringify(sessObj.matchedStops);
  }

  var logfile = 'app/logs/' + sessObj.phoneHash + '.log';
  var log = 'At ' + sessObj.startTime + ', user asked: \'' + sessObj.query + '\'.\n'
          + 'At ' + logtime.toDate() + ' (' + timeDiff + ' milliseconds later), we responded: \'' + message + '\'.\n'
          + 'Total conversation time: ' + totalTimeDiffString + '\n'
          + '  phoneHash: ' + sessObj.phoneHash + '\n'
          + '  routeNo: ' + sessObj.routeNo + '\n'
          + '  querySansBus: ' + sessObj.querySansBus + '\n'
          + '  definitiveStop: ' + sessObj.definitiveStop + '\n'
          + '  matchedStops: ' + matchedStops + '\n'
          + '  filteredStops: ' + JSON.stringify(sessObj.filteredStops) + '\n'
          + '  culledStops: ' + JSON.stringify(sessObj.culledStops) + '\n'
          + '  duplicateStops: ' + JSON.stringify(sessObj.duplicateStops) + '\n'
          + '  reasonForConvo: ' + sessObj.reasonForConvo + '\n'
          + '  tries: ' + sessObj.tries + '\n'
          + '  startTime: ' + sessObj.startTime + '\n'
          + '  convoStartTime: ' + sessObj.convoStartTime + '\n'
          + '----------\n';

  fs.appendFileSync(logfile, log);
//  console.log(log);
  console.log('Logged for ' + sessObj.phoneHash);
}

exports.logErr = function (message) {
  var logtime = moment(new Date());
  var log = logtime.toDate() + ': ' + message + '\n';
  fs.appendFileSync('app/logs/errors.log', log);
  console.log('Error logged: ' + message);
}

exports.logConnection = function (message) {
  var logtime = moment(new Date());
  var log = logtime.toDate() + ': ' + message + '\n';
  fs.appendFileSync('app/logs/mongolab.log', log);
//  console.log('Connection logged.');

}
