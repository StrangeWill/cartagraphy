// Dependencies:
var compare = require('./comparePredictions.js'),
    routeValidator = require('./routeValidator.js'),
    moment = require('moment')
    ;

var routePattern = new RegExp(/(route|rt|bus|line|the|#|number|no)\.* *(\d+)([a-zA-Z]?)/i),
    routePatternNoIdentifier = new RegExp(/(^|\s)(\d+)([a-zA-Z]?)/i),
    namedPattern = new RegExp(/(^|\s+)(shuttle|shut)($|\s)/i)
    ;

var requestParser = function (phoneHash, query, thisIsAConvo, cb) {
  if (thisIsAConvo) {
    // If we have a conversation, set the query to whatever they asked before
    // plus the new query from the current message:
    thisIsAConvo.origQuery = query;
    query = thisIsAConvo.querySansBus + ' ' + query;
  }

  var routeNo, stops;

  findRoute(query, thisIsAConvo, function (err, detectedRoute) {
    if (err) { throw err; }

    if (thisIsAConvo) {
      routeNo = detectedRoute ? detectedRoute : thisIsAConvo.routeNo;
    } else {
      routeNo = detectedRoute;
    }
    findStops(query, function (err, possibleStops) {
      if (err) { throw err; }

      compare(possibleStops, function (err, topStops) {
        if (err) { throw err; }

        stops = topStops;
      });

    });

  });

  var sessionObj;
  // Grab existing sessionObj, otherwise create it:
  if (thisIsAConvo) {
    sessionObj = thisIsAConvo.toObject();
    sessionObj.query = thisIsAConvo.origQuery;
    sessionObj.startTime = moment(new Date()).toDate();
    // Update the object query to the latest version (adding newer query):
    sessionObj.querySansBus = query.replace(routePattern, '');
    // Update the object list of matching stops:
    sessionObj.matchedStops = stops;
    // If we have a route number, update it in the sessionObj:
    // (It’s OK to replace it, because user clearly wants to change it.)
    if (routeNo) {
      sessionObj.routeNo = routeNo;
    }
    // Reset routeHints: (No need to include last session’s hints in this session’s logging.)
    sessionObj.routeHints = null;
    // Remove _id property because mongo is having non-a-that:
    delete sessionObj._id;
  } else {
    sessionObj = {
      phoneHash: phoneHash,
      routeNo: routeNo,
      routeHints: null,
      querySansBus: query.replace(routePattern, ''),
      definitiveStop: null,
      matchedStops: stops,
      filteredStops: null,
      culledStops: null,
      duplicateStops: null,
      reasonForConvo: '',
      tries: 1,
      query: query,
      startTime: moment(new Date()).toDate(),
      convoStartTime: moment(new Date()).toDate()
    }
  }

  if (sessionObj.matchedStops.length === 1) {
    sessionObj.definitiveStop = sessionObj.matchedStops[0];
  }

  if (sessionObj.routeNo) {
    routeValidator(sessionObj, function (err, sessionObj) {
      cb(err, sessionObj);
    });
  } else {
    cb(null, sessionObj);
  }
}

var findRoute = function (query, thisIsAConvo, cb) {
  var matchResults = query.match(routePattern),
      matchShuttle,
      detectedRoute
      ;

  // If regex doesn’t find a match, and there is a prior conversation
  // where hints were offered, then we try the regex without an identifier:
  if (!matchResults && thisIsAConvo && thisIsAConvo.toObject().routeHints) {
    matchResults = query.match(routePatternNoIdentifier);
  }

  // If neither regex finds a match, and there is no prior
  if (!matchResults && (!thisIsAConvo || !thisIsAConvo.toObject().routeNo)) {
    matchShuttle = query.match(namedPattern);
    if (matchShuttle && matchShuttle[2]) {
      detectedRoute = 'shuttle';
    } else {
      detectedRoute = undefined;
    }

  // If we found a match and there is a modifier (letter after route number):
  } else if (matchResults && matchResults[3]) {

    detectedRoute = matchResults[2] + matchResults[3];

  // If we found match and there is no modifier:
  } else if (matchResults) {
    detectedRoute = matchResults[2];

  // If we didn’t find a route number:
  } else {
    detectedRoute = undefined;
  }

  if (typeof (cb) === 'function') {
    cb(null, detectedRoute);
  }

}

var findStops = function (query, cb) {
  var querySansBus = query.replace(routePattern, '');
  // add ordinals to street numbers if needed
  // currently adds ordinals to the end of all numbers; would be better to match only 1 and 2-digit numbers
  querySansBus = querySansBus.replace(/(^|\s)([0-9]?)0(\s|$)/g, '$1$20th$3');
  querySansBus = querySansBus.replace(/(^|\s)([0-9]?)1(\s|$)/g, '$1$21st$3');
  querySansBus = querySansBus.replace(/(^|\s)([0-9]?)2(\s|$)/g, '$1$22nd$3');
  querySansBus = querySansBus.replace(/(^|\s)([0-9]?)3(\s|$)/g, '$1$23rd$3');
  querySansBus = querySansBus.replace(/(^|\s)([0-9]?)([4-9])(\s|$)/g, '$1$2$3th$4');
//  console.log(querySansBus);
  if (typeof (cb) === 'function') {
    cb(null, global.stopsClassifier.getClassifications(querySansBus));
  }
}

module.exports = requestParser;
