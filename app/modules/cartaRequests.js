var http   = require('http'),
    xml2js = require('xml2js'),
    moment = require('moment'),
    twiML  = require('./twiML.js'),
    logger = require('./logger.js'),
    conversation = require('./conversation.js'),
    format = require('string-template'),
    responses = require('../templates/responses.js')
    ;

// Create toTitleCase() method for string manipulation.
// (why this doesn’t already exist will forever elude me)
String.prototype.toTitleCase = function () {
    return this.replace(/\w\S*/g, function (txt) {return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
};

// =============================================================================
// ||
// ||     Requests made to CARTA API live inside the cartaRequests object
// ||
// =============================================================================

var cartaRequests = {
  // PREDICT BUS ARRIVALS
  // =============================================================================
  predict: function (sessionObj, res) {
    var requestType = '/getpredictions',
        predictions
        ;

    // Set the query parameter…
    var queryParam = 'rt=';
    // If the user is requesting the shuttle, set `rt` parameter to shuttle route numbers:
    if (sessionObj.routeNo === 'SHUTTLE') {

      global.shuttleRoutes.forEach(function (routeID, i) {
        queryParam += routeID;
        queryParam += i === global.shuttleRoutes.length - 1 ? '' : ',';
      });

    // If the user is not requesting the shuttle, set `rt` parameter to whatever route number we have:
    } else {
      queryParam += sessionObj.routeNo;
    }

    // Set `stpid` parameter:
    queryParam += '&stpid=' + sessionObj.definitiveStop;

    // If there are duplicateStops, add them to the request parameters:
    queryParam += sessionObj.duplicateStops ? ',' + sessionObj.duplicateStops[sessionObj.definitiveStop] : '';

    cartaRequests.request(requestType, queryParam, function (err, result) {
      if (result.prd) {
        predictions = result.prd;
        constructPredictionMessage(predictions, sessionObj.routeNo === 'SHUTTLE', function (err, message) {
          conversation.sendMessage(message, sessionObj, res);
       });
      } else {
        constructErrorMessage(result, function (err, message) {
          conversation.sendMessage(message, sessionObj, res);
        });
      }
    });
  },

  request: function (requestType, queryParam, cb) {
    // Set a bunch of default variables for use in the request to the CARTA API:
    var APIkey = require('../keys/carta.js'),
        keyParam = 'key=' + APIkey,
        host = 'bustracker.gocarta.org',
        path = '/bustime/api/v1',
        port = 80,
        method = 'GET'
        ;

    var options = {
      host: host,
      path: path + requestType + '?' + keyParam + '&' + queryParam,
      port: port,
      method: method
    }

    var result;

    // Make request to CARTA API:
    var request = http.request(options, function (response) {
      var body = '';
      response.on('data', function (data) {
        body += data;
      });

      // Do something with the response from the API:
      response.on('end', function () {
        // Turn XML response into JS object and do something with the result:
        var parser = new xml2js.Parser();
        parser.parseString(body, function (err, parsedResponse) {
          result = parsedResponse['bustime-response'];
          if (typeof (cb) === 'function') {
            cb(null, result);
          }
        });
      });
    });

    // If CARTA API responds with HTTP error, log it to the console:
    request.on('error', function (e) {
      logger.logErr('There was a problem with this request to the CARTA API: ' + e.message);
    });
    request.end();
  }

  // Future bus tracking services can be added to the cartaRequests object here
  // =============================================================================
}

// =============================================================================
// ||
// ||                     Prediction-specific utilities
// ||
// =============================================================================

// PREDICTION ERROR
// =============================================================================
// (returns boolean for whether the response from CARTA contains an error)
var predictionError = function (response) {
  return response.error ? true : false;
}

// CONSTRUCT ERROR MESSAGE
// =============================================================================
// (turns error message from CARTA Prediction response into a message string)
var constructErrorMessage = function (response, cb) {
  var message;
  if (response.error[0].rt) {
    message = response.error[0].msg[0]
                + ' for route #'
                + response.error[0].rt[0]
                + ' at '
                + global.stopsCollection[response.error[0].stpid[0]].stop_name.toTitleCase();
  } else {
    message = response.error[0].msg[0];
  }
  if (typeof (cb) === 'function') {
    cb(null, message);
  }
}

// CONSTRUCT PREDICTION
// =============================================================================
// (turns predictions from CARTA response into a message string)
var constructPredictionMessage = function (response, thisIsAShuttle, cb) {
  // Set some variables:
  var allPredictions = response;
  var now = moment(new Date());

  // Sort predictions by destination and store them in an object:
  var sortedPredictions = sortPredictions(allPredictions);

  var constructedMessage = '';

  // For each route in the predictions…
  for (var route in sortedPredictions) {
    // Check that route is not part of prototype:
    if (sortedPredictions.hasOwnProperty(route)) {
      var firstDestination = true;

      // For each destination in this route…
      for (var destination in sortedPredictions[route]) {
        // Check that destination is not part of prototype:
        if (sortedPredictions[route].hasOwnProperty(destination)) {
          var predictions = sortedPredictions[route][destination];
          var stop = global.stopsCollection[predictions[0].stpid[0]].stop_name.toTitleCase();
          var template = firstDestination ? responses.predictions.firstDestination : responses.predictions.secondDestination;
          var message, amount, verb, eta;

          if (predictions.length === 1) { // If there is only one prediction, construct the message as follows:
            amount = integerToWord(predictions.length);

            // Create a moment for this prediction’s ETA to compare with now:
            var prdtm = moment(predictions[0].prdtm, 'YYYYMMDD HH:mm');

            // Check time difference between ETA and now; finish message accordingly:
            verb = prdtm.diff(now, 'minutes') === 0 ? responses.predictions.verbs.present.single : responses.predictions.verbs.future;
            eta = prdtm.diff(now, 'minutes') === 0 ? 'now' : 'in ' + prdtm.diff(now, 'minutes') + 'm';

            message = format(template, {
              amount: amount,
              route: '#' + route,
              type: thisIsAShuttle ? ' shuttle' : ' bus',
              destination: destination,
              verb: verb,
              stop: stop,
              eta: eta
            });

            // And we’re done constructing the message for a single prediction!
            // =============================================================================

          } else { // If there are multiple predictions, we construct the message differently:
            // Create a moment for first prediction’s ETA to compare with now:
            var firstPrdtm = moment(predictions[0].prdtm, 'YYYYMMDD HH:mm'),
                predictionTimes = []
                ;

            if (firstPrdtm.diff(now, 'minutes') === 0) { // If the first prediction’s ETA is less than 1m from now:
              amount = 'One';

              verb = responses.predictions.verbs.present.single;
              eta = 'now';

              message = format(template, {
                amount: amount,
                route: '#' + route,
                type: thisIsAShuttle ? ' shuttle' : ' bus',
                destination: destination,
                verb: verb,
                stop: stop,
                eta: eta
              });

              // Now, remove first prediction from array:
              predictions.shift()
              predictions.forEach(function (prd, index) {
                var prdtm = moment(prd.prdtm, 'YYYYMMDD HH:mm');
                predictionTimes[index] = prdtm.diff(now, 'minutes') + 'm';
              });

              // And lump the rest of the predictions together:
              message += '; '
              message += format(responses.predictions.more, {
                amount: integerToWord(predictions.length).toLowerCase(),
                eta: 'in ' + predictionTimes.join(', ')
              });

            } else { // If the first prediction’s ETA is 1m or more from now:
              amount = integerToWord(predictions.length);
              verb = responses.predictions.verbs.future;

              predictions.forEach(function (prd, index) {
                var prdtm = moment(prd.prdtm, 'YYYYMMDD HH:mm');
                predictionTimes[index] = prdtm.diff(now, 'minutes') + 'm';
              });

              message = format(template, {
                amount: amount,
                route: '#' + route,
                type: thisIsAShuttle ? ' shuttles' : ' buses',
                destination: destination,
                verb: verb,
                stop: stop,
                eta: 'in ' + predictionTimes.join(', ')
              });
            }

            // And we’re done constructing the message for a multiple predictions!
            // =============================================================================
          }

          // Add period and space to end of message, to separate from next destination:
          constructedMessage += message + '. ';

          // And we’re done constructing the message for this destination!
          // =============================================================================

          // Make sure the next destination doesn’t repeat the route # and stop name:
          firstDestination = false;
        }
      }
    }
  }

  // All predictions for all destinations have been added!
  // Remove trailing period and space:
  var regexp = new RegExp(/. $/);
  constructedMessage = constructedMessage.replace(regexp, '');

  if (typeof (cb) === 'function') {
    cb(null, constructedMessage);
  }
}

// SORT PREDICTIONS BY DIRECTION
// =============================================================================
// (puts predictions into an object where each destination is a key whose value
//  is an array of predictions; used to construct a better response message)
var sortPredictions = function (predictions) {
  var sortedPredictions = {};
  predictions.forEach(function (prediction, i) {
    if (!sortedPredictions[prediction.rt[0]]) {
      sortedPredictions[prediction.rt[0]] = {};
    }
    if (!sortedPredictions[prediction.rt[0]][prediction.des[0].toTitleCase()]) {
      sortedPredictions[prediction.rt[0]][prediction.des[0].toTitleCase()] = [ prediction ];
    } else {
      sortedPredictions[prediction.rt[0]][prediction.des[0].toTitleCase()].push(prediction);
    }
  });
  return sortedPredictions;
}

// =============================================================================
// ||
// ||     Other utilities for use across multiple cartaRequests methods:
// ||
// =============================================================================

// INTEGER-TO-WORD
// =============================================================================
// (turns any number between 0 and 19 into a word; useful to avoid confusion
//  with bus route numbers, e.g. “Two #4 buses”, as opposed to “2 #4 buses”)
var integerToWord = function (integer) {
  var units = new Array ('Zero', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen');
  var theword = '';
  var started;

  if (integer > 19) { return 'Lots of'; }
  if (integer === 0) { return units[0]; }

  for (var i = 1; i < 20; i ++) {
    if (integer === i) {
      theword += (started ? units[i].toLowerCase() : units[i]);
    }
  }

  return theword;
}

module.exports = cartaRequests;
