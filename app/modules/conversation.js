var mongoose = require('mongoose'),
    PredictionConvos = require('../models/predictionConvoSchema.js'),
    twiML = require('./twiML.js'),
    logger = require('./logger.js'),
    moment = require('moment'),
    format = require('string-template'),
    responses = require('../templates/responses.js'),
    config = require('../../config.js')
    ;

// Create toTitleCase() method for string manipulation.
String.prototype.toTitleCase = function () {
  this.replace(/\w\S*/g, function (txt) {return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
  return this;
};

var conversation = {

  check: function (phoneHash, cb) {
    var parent = this;
    PredictionConvos.findOne({ phoneHash: phoneHash }, function (err, convo) {
      if (err) { console.log(err); }
      if (convo) {
        var now = moment(new Date());
        var lastConvoTime = moment(convo.toObject().startTime);
        if (now.diff(lastConvoTime, 'seconds') <= config.convoExpiration) {
          if (typeof (cb) === 'function') {
            cb(null, convo);
          }
        } else {
          if (typeof (cb) === 'function') {
            conversation.remove(convo.toObject(), function (err) {
              cb(null, false);
            })
          }
        }
      } else {
        if (typeof (cb) === 'function') {
          cb(null, false);
        }
      }
    });
  },

  create: function (sessionObj, cb) {
    // Create a document to keep track of this conversation
    var currentConvo = new PredictionConvos(sessionObj);

    currentConvo.save(function (err) {
      if (typeof (cb) === 'function') {
        cb(err, sessionObj);
      }
    });
  },

  update: function (sessionObj, cb) {
    sessionObj.tries ++;
    // Update document to keep track of this conversation
    PredictionConvos.update({ phoneHash: sessionObj.phoneHash }, sessionObj, function (err, numAffected) {
      if (typeof (cb) === 'function') {
        cb(err, sessionObj);
      }
    });
  },

  remove: function (sessionObj, cb) {
    PredictionConvos.findOneAndRemove({ phoneHash: sessionObj.phoneHash }, function (err) {
      if (typeof (cb) === 'function') {
        cb(err, sessionObj);
      }
    });
  },

  probeUser: function (sessionObj, res) {   // ask the rider a question to clarify their request
    var message;

    // We probe the user at multiple spots in the process, so we use the latest, most narrowed down array of stops:
    var stopsToProbeAbout = sessionObj.culledStops || sessionObj.filteredStops || sessionObj.matchedStops;

    // Figure out what needs to be clarified,
    // construct message accordingly:
    if (!sessionObj.routeNo) {
      if (sessionObj.routeHints) {
        var routeHintsString = '';
        sessionObj.routeHints.forEach(function (hint, i) {
          routeHintsString += i === sessionObj.routeHints.length - 1 ? 'or ' : '';
          routeHintsString += hint;
          routeHintsString += i !== sessionObj.routeHints.length - 1 ? ', ' : '';
        });
        message = format(responses.probes.clarifyRoute, {
          suggestedRoutes: routeHintsString
        });
      } else if (sessionObj.validRoute === false) {
      //  message = 'Your route number isn’t clear to me. Say something like ‘bus 4’ or ‘#9’.';
        message = format(responses.probes.invalidRoute, {
          validRoutes: global.validRoutesConvo
        });
      } else {
        message = responses.probes.unclearRoute;
      }
    } else if (sessionObj.reasonForConvo === 'no service') {
      message = responses.probes.noService;
    } else if (stopsToProbeAbout.length <= config.maxSuggestedStops) {
      var stopsHintsString = '';
      stopsToProbeAbout.forEach(function (stop, i) {
        stopsHintsString += i === stopsToProbeAbout.length - 1 ? 'or ' : '';
        stopsHintsString += global.stopsCollection[stop].stop_name.toTitleCase();
        stopsHintsString += i !== stopsToProbeAbout.length - 1 ? ', ' : '';
      });
      // Construct question for user:
      message = format(responses.probes.clarifyStop, {
        suggestedStops: stopsHintsString
      });
    } else if (stopsToProbeAbout.length > config.maxSuggestedStops) {
      // message = 'I’m having trouble identifying your stop. Try to be more specific.';
      message = responses.probes.tooManyStops;
    }
    conversation.sendMessage(message, sessionObj, res);
  },

  referUser: function (sessionObj, res) {   // failure condition; refer the rider to the CARTA helpline
    var message;
    if (sessionObj.reasonForConvo === 'generic') {
      message = format(responses.referrals.generic, {
        phone: config.CARTAvoicePhone
      });
    } else {
      // Construct message referring user to CARTA live phone support:
      message = format(responses.referrals.specific, {
        reason: sessionObj.reasonForConvo,
        phone: config.CARTAvoicePhone
      });
    }
    conversation.sendMessage(message, sessionObj, res);
  },

  // generic copyediting to make the message we send cleaner and more readable
  copyeditMessage: function (message, cb) {
    // replace stop numbers with stop names
    var stopPattern = /(.*)stop \#(\d*)(\D|$)/,
        editedMessage
        ;

    if (message.search(stopPattern) >= 0) {
      var stop = message.replace(stopPattern, '$2');
      var stopName = global.stopsCollection[stop].stop_name.toTitleCase();
      editedMessage = message.replace(stopPattern, '$1' + stopName);
    } else {
      editedMessage = message;
    }
    // remove improperly capitalized At or And
    var badCaps = /( )A(t |nd )/g;
    editedMessage = editedMessage.replace(badCaps, '$1a$2');
    editedMessage = editedMessage.replace(/Near\s/g, 'near ');
    // remove any stray + symbols from route directions
    editedMessage = editedMessage.replace(/\s\+\s/g, ' and ');
    // replace route 33 and 34 with their shuttle names
    editedMessage = editedMessage.replace(/(route )?\#33( shuttle(s)?)?/g, 'Downtown Shuttle$3');
    editedMessage = editedMessage.replace(/(route )?\#34( shuttle(s)?)?/g, 'North Shore Shuttle$3');
    // for northshore shuttle, replace 'Tremont and Frazier' destination
    if (editedMessage.search(/North Shore Shuttle/g) > 0) {
      editedMessage = editedMessage.replace(/Tremont and Frazier/, 'Tremont and Frazier (Northshore Garage)');
    }

    if (typeof (cb) === 'function') {
      cb(editedMessage);
    }
  },

  // take constructed message, send it through the copyeditor, then send it to the rider
  sendMessage: function (message, sessionObj, res) {
    conversation.copyeditMessage(message, function (editedMessage) {
      logger.log(sessionObj, editedMessage);
      res.send(twiML(editedMessage));
    });
  },

  makeBetterRouteList: function () {
    global.validRoutesConvo = global.validRoutesConvo.sort();
    global.validRoutesConvo = global.validRoutesConvo.join(', ');
    global.validRoutesConvo = global.validRoutesConvo.replace(/0(\d)\,/g, '$1,'); // strip the padding we added earlier to single-digit routes
    global.validRoutesConvo = global.validRoutesConvo.replace(/(\, (33|34))/g, ''); // replace routes 33 and 34 with Shuttle
    global.validRoutesConvo += ' and Shuttle'
  }
}

module.exports = conversation;
