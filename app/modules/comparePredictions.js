var fs = require('fs');

module.exports = function (possibleStops, cb) {
	//fs.writeFile('./app/data/' + querySansBus, JSON.stringify(possibleStops));
	var len = possibleStops.length;
	var valueMax = possibleStops[0].value;
	var valueMin = possibleStops[len - 1].value;
	var valueRatio = Math.round(valueMax / valueMin);
	// console.log(valueMax + ' / ' + valueMin + ' = ' + valueRatio);
	var topStops = [];
	var tempMax = valueMax;
	var k = 0;
	var scoreArray = []
	for (var i = 0; i < len; i ++) {
		if (possibleStops[i].value === valueMax) {
			topStops.push(possibleStops[i].label);
		}
		if (possibleStops[i].value === tempMax) {
			k ++;
		}
		if (possibleStops[i].value !== tempMax) {
			scoreArray.push([ k, tempMax ]);
			k = 0;
			tempMax = possibleStops[i].value;
		}
	}
	scoreArray.push([ k + 1, tempMax ]);
	if (typeof (cb) === 'function') {
		cb(null, topStops);
	}
	// console.log(topStops);
	// console.log(scoreArray);
	// if (topStops.length <= 3) {
	// 	return topStops;
	// } else {
	// 	return false;
	// }
	// var append = 'query: ' + querySansBus + '\nSets of stops: ' + scoreArray.length + '\nRatio between best and worst match: ' + valueRatio + '\n' + JSON.stringify(scoreArray) + '\n\n';
	// fs.appendFile('./app/data/scoreArrays.txt', append, function(err) {
	// 	if (err) throw err;
	// });
}
