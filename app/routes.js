var requestParser = require('./modules/requestParser.js'),
    cartaRequests = require('./modules/cartaRequests.js'),
    conversation  = require('./modules/conversation.js'),
    hashPhone     = require('./modules/hashPhone.js'),
    twiML         = require('./modules/twiML.js'),
    massageStops  = require('./modules/massageStops.js'),
    logger        = require('./modules/logger.js'),
    config        = require('../config.js')
    ;

module.exports = function (app, express) {
  var router = express.Router();        // get an instance of the express Router

  // middleware to use for all requests
  router.use(function (req, res, next) {
    // do logging
    next(); // make sure we go to the next routes and don't stop here
  });

  // test route to make sure everything is working (accessed at GET http://localhost:8080/api)
  router.get('/', function (req, res) {
    res.json({ message: 'Alright, alright, alright! Welcome to CARTAgraphy!' });
  });

  // more routes for our API will happen here
  router.route('/incoming')
    .post(function (req, res) {

      // =============================================================================
      // ||
      // ||                            Preliminary stuff
      // ||
      // =============================================================================
      // Set response type from the outset:
      // (so we can send responses from now on)
      res.set('Content-Type', 'text/xml');

      // Get information from the request:
      var query   = req.body.Body || req.body.message,
          phoneNo = req.body.From || req.body.phone,
          phoneHash;

      // Does the request include a phone number?
      if (phoneNo) {
//        var phoneHash = hashPhone(phoneNo);
        phoneHash = phoneNo; // FOR TESTING ONLY; REPLACE WITH HASHED PHONE IN PRODUCTION
      } else {
        // If we didn’t get a phone number, respond with error msg:
        logger.logErr('No phone number attached to this request.');
        res.send(twiML('No phone number attached to this request.'));
      }

      // Does the request include a query?
      if (!query) {
        // If we didn’t get a message, respond with error msg:
        logger.logErr('No message attached to this request.');
        res.send(twiML('No message attached to this request.'));
      }

      // Quick log to the console while we develop…
      console.log(phoneHash + ' is asking: ' + query);

      // =============================================================================
      // ||
      // ||                       Prepare for application flow
      // ||
      // =============================================================================

      // First we check whether we already have a conversation with this user:
      conversation.check(phoneHash, function (err, convo) {
        if (err) { throw err; }

        var thisIsAConvo = convo;

        // Now we parse the request:
        requestParser(phoneHash, query, thisIsAConvo, function (err, sessionObj) {

          // =============================================================================
          // ||
          // ||                      We don’t have a route number…
          // ||
          // =============================================================================

          // If we don’t have a route number…
          if (!sessionObj.routeNo) {
            // …and there’s a prior conversation…
            if (thisIsAConvo) {
              // and the original conversation didn’t have a route number:
              if (!sessionObj.routeNo) {
                conversation.remove(sessionObj, function (err, sessionObj) {
                  sessionObj.reasonForConvo = 'route number';
                  conversation.referUser(sessionObj, res);
                });
              // and the original conversation _did_ have a route number:
              } else {
                var routeNo = sessionObj.routeNo;
              }
            // and there was no prior conversation:
            } else {
              sessionObj.tries = 0; // Give user an extra try when no bus route is identified.
              conversation.create(sessionObj, function (err, sessionObj) {
                conversation.probeUser(sessionObj, res);
              });
            }

          // =============================================================================
          // ||
          // ||                         We got a route number!
          // ||
          // =============================================================================

          } else {

            // … and there’s a prior conversation:
            if (thisIsAConvo) {
              conversation.update(sessionObj);
            }

            // =============================================================================
            // ||
            // ||                      We got a single stop match!
            // ||
            // =============================================================================

            // If requestParser() returns a single stop, we submit request to CARTA API:
            if (sessionObj.definitiveStop) {
              // Make request to CARTA:
              cartaRequests.predict(sessionObj, res);
              // If this happens to be part of a conversation,
              // remove the conversation from the DB:
              if (thisIsAConvo) {
                conversation.remove(sessionObj);
              }

            // =============================================================================
            // ||
            // ||        We got fewer than twelve matches, so we massage the stops
            // ||
            // =============================================================================

            } else if (sessionObj.matchedStops.length <= 12) {

              // Run stops through routes filter:
              massageStops.filter(sessionObj, function (err, sessionObj) {

                // FILTERING FOUND ONE STOP:
                // =============================================================================
                if (sessionObj.definitiveStop) {
                  cartaRequests.predict(sessionObj, res);
                  // If this happens to be part of a conversation,
                  // remove the conversation from the DB:
                  if (thisIsAConvo) {
                    conversation.remove(sessionObj);
                  }

                } else {

                  // FILTERING DID NOT FIND A DEFINITIVE STOP, SO WE CHECK IF IT RETURNED ZERO:
                  // =============================================================================
                  if (sessionObj.filteredStops.length === 0) {
                    sessionObj.reasonForConvo = 'no service';
                    if (thisIsAConvo) {
                      if (sessionObj.tries <= config.maxTries) {
                        conversation.update(sessionObj, function (err, sessionObj) {
                          conversation.probeUser(sessionObj, res);
                        });
                      } else {
                        conversation.remove(sessionObj, function (err, sessionObj) {
                          sessionObj.reasonForConvo = 'generic';
                          conversation.referUser(sessionObj, res);
                        });
                      }
                    } else {
                      conversation.create(sessionObj, function (err, sessionObj) {
                        conversation.probeUser(sessionObj, res);
                      });
                    }

                  // FILTERING RETURNED MORE THAN ONE STOP, SO WE COMPARE AND CULL THE STOPS:
                  // =============================================================================
                  } else {
                    massageStops.findDuplicates(sessionObj, function (err, sessionObj) {

                      // If culling the stops leaves only one stop, submit request to CARTA API:
                      if (sessionObj.definitiveStop) {
                        cartaRequests.predict(sessionObj, res);
                        // If this happens to be part of a conversation,
                        // remove the conversation from the DB:
                        if (thisIsAConvo) {
                          conversation.remove(sessionObj);
                        }

                      // If after culling the stops we still have two or three stops,
                      } else if (sessionObj.culledStops.length <= 3) {

                        // If this is a conversation and there have been fewer than max allowed number of tries,
                        // update the conversation session and probe user for more info:
                        if (thisIsAConvo) {
                          if (sessionObj.tries < config.maxTries) {
                            conversation.update(sessionObj, function (err, sessionObj) {
                              conversation.probeUser(sessionObj, res);
                            });

                          // If this is a conversation and we’ve exceeded max allowed number of tries,
                          // remove the conversation session and refer user to CARTA live support:
                          } else {
                            conversation.remove(sessionObj, function (err, sessionObj) {
                              sessionObj.reasonForConvo = 'stop';
                              conversation.referUser(sessionObj, res);
                            });
                          }

                        // If this is not a conversation, create a conversation session
                        // and probe user for more info:
                        } else {
                          conversation.create(sessionObj, function (err, sessionObj) {
                            conversation.probeUser(sessionObj, res);
                          });
                        }

                      } else {
                        // If this is a conversation and there have been fewer than max allowed number of tries,
                        // update the conversation session and probe user for more info:
                        if (thisIsAConvo) {
                          if (sessionObj.tries < config.maxTries) {
                            conversation.update(sessionObj, function (err, sessionObj) {
                              conversation.probeUser(sessionObj, res);
                            });

                          // If this is a conversation and we’ve exceeded max allowed number of tries,
                          // remove the conversation session and refer user to CARTA live support:
                          } else {
                            conversation.remove(sessionObj, function (err, sessionObj) {
                              sessionObj.reasonForConvo = 'stop';
                              conversation.referUser(sessionObj, res);
                            });
                          }

                        // If this is not a conversation, create the conversation session
                        // and probe user for more info:
                        } else {
                          conversation.create(sessionObj, function (err, sessionObj) {
                            conversation.probeUser(sessionObj, res);
                          });
                        }
                      }
                    });
                  }
                }
              });

            // =============================================================================
            // ||
            // ||            We got more than twelve matches, so we probe user
            // ||
            // =============================================================================

            } else {
              // If this is a conversation and there have been fewer than max allowed number of tries,
              // update the conversation session and probe user for more info:
              if (thisIsAConvo) {
                if (sessionObj.tries < config.maxTries) {
                  conversation.update(sessionObj, function (err, sessionObj) {
                    conversation.probeUser(sessionObj, res);
                  });

                // If this is a conversation and we’ve exceeded max allowed number of tries,
                // remove the conversation session and refer user to CARTA live support:
                } else {
                  conversation.remove(sessionObj, function (err, sessionObj) {
                    sessionObj.reasonForConvo = 'stop';
                    conversation.referUser(sessionObj, res);
                  });
                }

              // If this is not a conversation, create the conversation session
              // and probe user for more info:
              } else {
                conversation.create(sessionObj, function (err, sessionObj) {
                  conversation.probeUser(sessionObj, res);
                });
              }
            }

          }

        });

      });

    });

  // REGISTER OUR ROUTES -------------------------------
  // all of our routes will be prefixed with /api
  app.use('/api', router);
}
