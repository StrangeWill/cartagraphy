/* jshint quotmark:double */
var responses = {
  predictions: {
    firstDestination: "{amount} {route}{type} headed to {destination} {verb} at {stop} {eta}",
    secondDestination: "{amount} headed to {destination} {verb} {eta}",
    more: "{amount} more {eta}",
    verbs: {
      present: {
        single: "is arriving",
        multiple: "are arriving"
      },
      future: "will arrive"
    }
  },

  probes: {
    clarifyRoute: "Did you mean {suggestedRoutes}?",
    invalidRoute: "That isn't a valid route. I know about these routes: {validRoutes}. Try one of those.",
    unclearRoute: "Your route number isn't clear to me. Say something like 'bus 4', '#9' or 'Shuttle'.",
    noService: "None of the stops we found are served by the route number you specified. Try a different route number or stop.",
    clarifyStop: "Did you mean {suggestedStops}? Text back the cross street or landmark.",
    tooManyStops: "I've found lots of stops that might match yours. Can you tell me a nearby cross street?"
  },

  referrals: {
    generic: "I'm sorry I'm unable to help you. Please call CARTA at {phone} for assistance.",
    specific: "I'm sorry I'm unable to identify your {reason}. Please call CARTA at {phone} for assistance."
  }
};

module.exports = responses;
