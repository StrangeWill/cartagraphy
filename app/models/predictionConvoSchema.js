var mongoose = require('mongoose');

var predictionConvoSchema =  new mongoose.Schema({
  phoneHash: String,
  routeNo: String,
  routeHints: [ String ],
  querySansBus: String,
  definitiveStop: Number,
  matchedStops: [ Number ],
  filteredStops: [ Number ],
  culledStops: [ Number ],
  duplicateStops: Object,
  reasonForConvo: String,
  tries: Number,
  startTime: Date,
  convoStartTime: Date,
  originalQuery: String
});

var predictionConvo = module.exports = mongoose.model('predictionConvos', predictionConvoSchema);
