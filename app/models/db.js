// Bring Mongoose into the app
var mongoose = require( 'mongoose' );

var logger = require('../modules/logger.js');

var uriUtil = require('mongodb-uri'),
    mongolab = require('../keys/mongolab.js'),
    mongodbUri = 'mongodb://' + mongolab.user + ':' + mongolab.pass + '@ds039860.mongolab.com:39860/cartagraphy',
    mongooseUri = uriUtil.formatMongoose(mongodbUri);

var options = { server: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } }, replset: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } } };

// Create the database connection
mongoose.connect(mongooseUri, options);

// CONNECTION EVENTS
// When successfully connected
mongoose.connection.on('connected', function () {
  logger.logConnection('Connected.');
  console.log('Mongoose default connection open to MongoLab.');
});

// If the connection throws an error
mongoose.connection.on('error', function (err) {
  logger.logErr('Mongoose default connection error: ' + err);
  logger.logConnection('Mongoose default connection error: ' + err);
  console.log('Mongoose default connection error: ' + err);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
  logger.logConnection('Disconnected.');
  console.log('Mongoose default connection disconnected.');
});

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', function () {
  mongoose.connection.close(function () {
    logger.logConnection('Disconnected through app termination.');
    console.log('Mongoose default connection disconnected through app termination.');
    process.exit(0);
  });
});
